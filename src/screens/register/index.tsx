import React, { useContext, useState } from 'react';
import {
  ActivityIndicator, Text, TouchableOpacity, View,
} from 'react-native';
import { StackNavigationProp } from '@react-navigation/stack';
import { StackParams } from '../types';
import styles from './styles';
// eslint-disable-next-line import/no-unresolved
import FictionFloatingLabelInput from '../../libs/floatingLabel';
import Context from '../../store/Context';
import SessionManager from '../../managers/SessionManager';
import { register } from '../../api/auth';
import User from '../../entities/User';
import Alert from '../../components/Alert';


type Props = {
  navigation: StackNavigationProp<StackParams>;
};

const Register: React.FC<Props> = ({ navigation }) => {
  const [loggingIn, setLoggingIn] = useState(false);
  const [name, setName] = useState(undefined);
  const [password, setPassword] = useState(undefined);

  const { dispatch } = useContext(Context);

  const handleNavToLogIn = async () => {
    navigation.navigate('Login');
  };

  const handleRegister = async () => {
    setLoggingIn(true);
    if (password && name) {
      const user: User = await register(name, password);
      if (user) {
        try {
          dispatch({ type: 'setUser', payload: user });
          await SessionManager.storeSession(user);
          navigation.navigate('BooksList');
        } catch (e) {
          Alert('Error ', e);
        }
      }
    }
    setTimeout(() => setLoggingIn(false), 500);
  };
  return (
    <View style={styles.container}>
      <View style={styles.inputsContainer}>
        <FictionFloatingLabelInput
          label="Name"
          value={name} // just a state variable
          onChangeText={(t) => setName(t)}
          containerStyle={styles.input}
        />
        <FictionFloatingLabelInput
          containerStyle={styles.input}
          label="Password"
          value={password}
          onChangeText={(t) => setPassword(t)}
        />
      </View>
      <View style={styles.glowContainer}>
        <TouchableOpacity onPress={() => handleRegister()} style={styles.loginButton}>
          {loggingIn ? <ActivityIndicator style={styles.activity} color="white" size="large" /> : (
            <Text style={styles.loginText}>
                Register
            </Text>
          )}
        </TouchableOpacity>
      </View>

      <TouchableOpacity style={styles.button} onPress={() => handleNavToLogIn()}>
        <Text style={styles.loginText}>Log In</Text>
      </TouchableOpacity>

    </View>
  );
};

export default Register;
