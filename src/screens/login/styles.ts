import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    flex: 1,
  },
  inputsContainer: {
    height: '20%',
    marginTop: '20%',
  },
  input: {
    borderColor: '#32a852',
  },
  glowContainer: {
    shadowColor: '#32a852',
    shadowOffset: { width: 3, height: 3 },
    shadowOpacity: 0.8,
    shadowRadius: 4,
    elevation: 3,
  },
  loginButton: {
    flexDirection: 'row',
    alignSelf: 'center',
    alignContent: 'center',
    marginTop: '10%',
    height: 56,
    width: '50%',
    backgroundColor: '#32a852',
    padding: 18,
    justifyContent: 'center',
    borderRadius: 99,
  },
  activity: {
    marginBottom: 24,
    alignSelf: 'center',
  },
  loginText: {
    fontSize: 16,
    color: '#FFFFFF',
  },
  button: {
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'grey',
    marginTop: 10,
    width: 70,
    height: 30,
    borderRadius: 10,
  },

});

export default styles;
