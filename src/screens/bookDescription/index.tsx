import React, { useContext, useEffect, useState } from 'react';
import { Text, View, ScrollView } from 'react-native';
import styles from './styles';
import { getBookById } from '../../api/books';
import Context from '../../store/Context';
import Book from '../../entities/Book';
import BookItem from '../../components/bookItem';
import Alert from '../../components/Alert';

type Props = {
  route: any;
};

const Description: React.FC <Props> = ({ route }) => {
  const { id } = route.params;
  const { store } = useContext(Context);

  const [book, setBook] = useState<Book>(undefined);

  const fetchBook = async () => {
    const resp = await getBookById(store.user.token, id);

    return resp;
  };
  useEffect(() => {
    const getOneBook = async () => {
      const res = await fetchBook();
      setBook(res.book);
    };
    getOneBook();
  }, [id]);

  return (
    <View style={styles.container}>
      {book && (
        <ScrollView style={styles.bookContainer}><BookItem title={book.title} onPress={() => Alert(`hours for read:${(book.pageCount / 30).toFixed(1)}`, `${book.author} is a great option`)} author={book.author} pageCount={book.pageCount} publisher={book.publisher} coverImageUrl={book.coverImageUrl} synopsis={book.synopsis} />
          <ScrollView style={styles.scroll}>
            <Text style={styles.text}>{book.synopsis}</Text>
          </ScrollView>
        </ScrollView>
      )}
    </View>
  );
};

export default Description;
