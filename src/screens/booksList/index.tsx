import React, { useContext, useEffect, useState } from 'react';
import {
  ActivityIndicator, FlatList, RefreshControl, View,
} from 'react-native';
// eslint-disable-next-line import/no-unresolved
import Icon from 'react-native-vector-icons/FontAwesome';
import { Input } from 'react-native-elements';
import { StackNavigationProp } from '@react-navigation/stack';
import styles from './styles';
import { StackParams } from '../types';
import { getBooks, searchBook } from '../../api/books';
import Context from '../../store/Context';
import BookItem from '../../components/bookItem';
import SessionManager from '../../managers/SessionManager';
import Alert from '../../components/Alert';

type Props = {
  navigation: StackNavigationProp<StackParams>;
};

const BooksList: React.FC <Props> = ({ navigation }) => {
  const [booksList, setBooksList] = useState(undefined);
  const [isRefreshing, setIsRefreshing] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [query, setQuery] = useState('');

  const { store } = useContext(Context);

  const fetchBooks = async () => {
    setIsRefreshing(true);
    const resp = await getBooks(store.user.token);
    if (resp) {
      setIsRefreshing(false);
    }
    return resp;
  };

  const fetchFilteredBooks = async () => {
    if (query.length > 0) {
      setIsLoading(true);
      const resp = await searchBook(store.user.token, query.toLowerCase());
      if (resp) {
        setBooksList(resp.books);
        setIsLoading(false);
      }
      return resp;
    }

    return Alert('What are you trying to find?', 'Seriously');
  };

  const checkIsValidated = async () => {
    const session = await SessionManager.getSession();
    if (session === undefined || !session.token) {
      navigation.navigate('Register');
    }
  };

  useEffect(
    () => navigation.addListener('transitionStart', () => {
      navigation.navigate('BooksList');
    }),
    [navigation],
  );

  useEffect(
    () => {
      const fetchAll = async () => {
        const res = await fetchBooks();
        setBooksList(res.data.books);
      };
      if (query.length === 0) {
        setTimeout(() => fetchAll(), 1000);
      }
    }, [navigation, query, store],
  );

  useEffect(
    () => {
      const validate = async () => { checkIsValidated(); };
      validate();
    },
    [],
  );

  return (
    <View style={styles.container}>

      <View style={styles.searchContainer}>

        <Input
          style={styles.input}
          placeholder="Search"
          onChangeText={(text) => {
            setQuery(text);
          }}
          rightIcon={(
            <Icon
              onPress={() => fetchFilteredBooks()}
              name="search-plus"
              size={24}
              color="#32a852"
            />
          )}
        />
      </View>
      {isLoading && <ActivityIndicator size="large" color="#32a852" />}
      {booksList !== undefined && (
        <FlatList
          style={styles.filtersContainer}
          data={booksList}
          refreshControl={(
            <RefreshControl
              refreshing={isRefreshing}
              onRefresh={() => fetchBooks()}
            />
          )}
          keyExtractor={(item) => item.id}
          renderItem={({ item }) => (
            <BookItem
              key={item.id}
              id={item.id}
              title={item.title}
              author={item.author}
              pageCount={item.pageCount}
              publisher={item.publisher}
              synopsis={item.synopsis}
              coverImageUrl={item.coverImageUrl}
              onPress={() => navigation.navigate('BookDescription', { id: item.id })}
            />
          )}
        />
      )}
    </View>
  );
};

export default BooksList;
