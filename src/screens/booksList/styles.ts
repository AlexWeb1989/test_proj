import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    alignItems: 'center',
    flex: 1,
  },
  filtersContainer: {
    opacity: 1,
  },
  searchContainer: {
    height: 30,
    width: '80%',
    marginTop: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
  input: {
    opacity: 0.6,
  },
});

export default styles;
