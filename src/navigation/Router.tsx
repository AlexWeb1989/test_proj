import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { Platform, StatusBar, View } from 'react-native';

import React from 'react';

import styles from './nav.styles';
import BooksList from '../screens/booksList';
import Login from '../screens/login';
import Description from '../screens/bookDescription';
import Register from '../screens/register';

const Stack = createStackNavigator();

export default function MainNavigatorStack(isAuthorized) {
  return (
    <NavigationContainer>
      <View style={styles.container}>
        {Platform.OS === 'ios' && <StatusBar barStyle="default" />}
        <Stack.Navigator
          initialRouteName={isAuthorized ? 'BooksList' : 'Register'}
          screenOptions={{
            headerStyle: {
              backgroundColor: '#32a852',
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
              fontWeight: 'bold',
            },
          }}
        >
          <Stack.Screen name="Register" component={Register} options={{ headerLeft: () => null }} />
          <Stack.Screen name="Login" component={Login} options={{ headerLeft: () => null }} />
          <Stack.Screen name="BooksList" component={BooksList} options={{ headerLeft: () => null }} />
          <Stack.Screen name="BookDescription" component={Description} />
        </Stack.Navigator>
      </View>
    </NavigationContainer>
  );
}
