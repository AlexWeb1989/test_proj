import React, { Component } from 'react';
import {
  View,
  Animated,
  TextInput,
  Dimensions,
} from 'react-native';

const { width: viewportWidth, height: viewportHeight } = Dimensions.get(
  'window',
);
const defaultStyles = {
  labelStyle: {
    position: 'absolute',
    left: 0,
    top: 0,
  },
  textInput: {
    fontSize: 14,
    color: '#000',
    paddingVertical: 10,
    marginTop: 3,
  },
  focusedTextInput: {
  },
  selectionColor: '#7165E3',
  defaultContainerStyle: {
    marginTop: viewportHeight * (2 / 100),
    alignSelf: 'center',
    width: viewportWidth * (90 / 100),
    backgroundColor: '#FFFFFF',
    borderRadius: 10,
    paddingHorizontal: 12,
  },

  defaultFocusedContainerStyle: {
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },

  defaultUnFocusedContainerStyle: {
    borderWidth: 1,
    borderColor: '#7165E3',
  },
  defaultSubContainerStyle: {
    // flexDirection:"row",
    // alignItems:"center",
    // width:"100%"
  },
};


class FictionFloatingLabelInput extends Component {
  // eslint-disable-next-line react/state-in-constructor
  state = {
    isFocused: false,
  };

  // eslint-disable-next-line react/no-deprecated
  componentWillMount() {
    // eslint-disable-next-line no-underscore-dangle
    this._animatedIsFocused = new Animated.Value(this.props.value === '' ? 0 : 1);
  }

  // eslint-disable-next-line react/sort-comp
  handleFocus = () => this.setState({ isFocused: true });

  handleBlur = () => this.setState({ isFocused: false });

  componentDidUpdate() {
    console.log('s');
    Animated.timing(this._animatedIsFocused, {
      toValue: (this.state.isFocused || this.props.value !== '') ? 1 : 0,
      duration: 200,
      useNativeDriver: false,
    }).start();
  }

  render() {
    const {
      label,
      containerStyle,
      focusedContainerStyle,
      unFocusedContainerStyle,
      subContainerStyle,
      focusedSubContainerStyle,
      unfocusedSubContainerStyle,
      selectionColor,
      labelStyle,
      focusedLabelStyle,
      unfocusedLabelStyle,
      textInputStyle,
      focusedTextInputStyle,
      unFocusedTextInputStyle,
      labelFocusedTop,
      labelUnFocusedTop,
      labelFontSizeUnFocused,
      labelFontSizeFocused,
      labelColorUnFocused,
      labelColorFocused,
      underlineColorAndroid,
      preOnFocus,
      preOnBlur,
      postOnFocus,
      postOnBlur,
      ...props
    } = this.props;
    const { isFocused } = this.state;
    const style = defaultStyles;
    const animatedLabelStyle = {
      top: this._animatedIsFocused.interpolate({
        inputRange: [0, 1],
        outputRange: [labelUnFocusedTop || 15, labelFocusedTop || 2],
      }),
      fontSize: this._animatedIsFocused.interpolate({
        inputRange: [0, 1],
        outputRange: [labelFontSizeUnFocused || 14, labelFontSizeFocused || 10],
      }),
      color: this._animatedIsFocused.interpolate({
        inputRange: [0, 1],
        outputRange: [labelColorUnFocused || '#8B8B8B', labelColorFocused || '#8B80F4'],
      }),
    };
    return (
      <View style={[
        style.defaultContainerStyle,

        isFocused ? style.defaultFocusedContainerStyle : style.defaultUnFocusedContainerStyle,
        containerStyle,
        isFocused ? focusedContainerStyle : unFocusedContainerStyle,
      ]}
      >
        {/* eslint-disable-next-line max-len */}
        <View style={[style.defaultSubContainerStyle, subContainerStyle, isFocused ? focusedSubContainerStyle : unfocusedSubContainerStyle]}>
          {/* eslint-disable-next-line max-len */}
          <Animated.Text style={[style.labelStyle, animatedLabelStyle, labelStyle, isFocused ? focusedLabelStyle : unfocusedLabelStyle]}>
            {label}
          </Animated.Text>
          {/* eslint-disable-next-line max-len */}
          <TextInput
            // eslint-disable-next-line react/jsx-props-no-spreading
            {...props}

            // eslint-disable-next-line max-len
            style={[style.textInput, isFocused && style.focusedTextInput, isFocused ? focusedTextInputStyle : unFocusedTextInputStyle, textInputStyle]}
            onFocus={() => {
              // eslint-disable-next-line no-unused-expressions
              preOnFocus && preOnFocus();
              this.handleFocus();
              // eslint-disable-next-line no-unused-expressions
              postOnFocus && postOnFocus();
            }}
            onBlur={() => {
              // eslint-disable-next-line no-unused-expressions
              preOnBlur && preOnBlur();
              this.handleBlur();
              // eslint-disable-next-line no-unused-expressions
              postOnBlur && postOnBlur();
            }}
            // eslint-disable-next-line no-unused-expressions,react/jsx-no-duplicate-props
            onBlur={this.handleBlur}
            blurOnSubmit
            selectionColor={selectionColor || style.selectionColor}
            underlineColorAndroid={underlineColorAndroid || 'transparent'}

          />
        </View>
      </View>
    );
  }
}

// eslint-disable-next-line import/prefer-default-export
export default FictionFloatingLabelInput;
