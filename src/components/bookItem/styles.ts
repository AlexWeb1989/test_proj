import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    flex: 1,
  },
  glowContainer: {
    shadowColor: '#32a852',
    shadowOffset: { width: 3, height: 2 },
    shadowOpacity: 0.2,
    shadowRadius: 5,
    elevation: 3,
    borderRadius: 20,
  },
  titleContainer: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
  },
  description: {
    marginBottom: 10,
  },
  descriptionWhite: {
    color: 'white',
  },
  image: {
    height: 230,
  },
});

export default styles;
