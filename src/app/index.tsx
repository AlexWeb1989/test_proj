import React, { useReducer, useState } from 'react';

import { AppLoading } from 'expo';
import { Asset } from 'expo-asset';
import * as Font from 'expo-font';
import MainNavigatorStack from '../navigation/Router';
import Context from '../store/Context';
import { initialState, reducer } from '../store/reducers';
import SessionManager from '../managers/SessionManager';

const Index: React.FC<any> = () => {
  const [store, dispatch] = useReducer(reducer, initialState);
  const [isAuthorized, setIsAuthorized] = useState(false);

  const [isLoadingComplete, setLoadingComplete] = useState(false);

  const processsAuth = async () => {
    const session = await SessionManager.getSession();
    if (session !== undefined && session.token) {
      dispatch({ type: 'setUser', payload: session });
      setIsAuthorized(true);
    } else {
      setIsAuthorized(false);
    }
  };

  const loadResourcesAsync = async () => {
    await Promise.all([
      Asset.loadAsync([
        // ...
      ]),
      Font.loadAsync({
        // ...
      }),
    ]);
  };

  const handleLoadingError = () => {
    // ...
  };

  const handleFinishLoading = async () => {
    await processsAuth();
    setTimeout(() => setLoadingComplete(true), 300);
  };

  if (!isLoadingComplete) {
    return (
      <AppLoading
        startAsync={loadResourcesAsync}
        onError={handleLoadingError}
        onFinish={handleFinishLoading}
      />
    );
  }
  return (
    <Context.Provider value={{ store, dispatch }}>
      <MainNavigatorStack isAuthorized={isAuthorized} />
    </Context.Provider>
  );
};

export default Index;
