import AsyncStorage from '@react-native-async-storage/async-storage';

const storeRes = {
  SUCCESS: { value: 0 },
  ERROR: { value: 1 },
};

const storeSession = async (session) => {
  try {
    const jsonString = JSON.stringify(session);
    await AsyncStorage.setItem('session', jsonString);
    return storeRes.SUCCESS;
  } catch (e) {
    console.log(e);
    return storeRes.ERROR;
  }
};

const getSession = async () => {
  try {
    const jsonString = await AsyncStorage.getItem('session');
    return jsonString != null ? JSON.parse(jsonString) : undefined;
  } catch (e) {
    return undefined;
  }
};

const removeSession = async () => {
  try {
    await AsyncStorage.removeItem('session');
    return storeRes.SUCCESS;
  } catch (e) {
    return storeRes.ERROR;
  }
};

export default { storeSession, getSession, removeSession };
