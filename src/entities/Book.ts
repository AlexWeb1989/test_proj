export default interface Book {
  title: string;
  author: string;
  coverImageUrl: string;
  id: number;
  pageCount: number;
  publisher: string;
  synopsis: string;
}
