
import RequestAPI from './Requests';

const register = async (username: string, password: string) => {
  const url = '/auth/register';
  const body = {
    username,
    password,
  };
  const { data } = await RequestAPI('post', url, body);
  return data.user;
};

const login = async (username: string, password: string) => {
  const url = '/auth/login';
  const body = {
    username,
    password,
  };
  const { data } = await RequestAPI('post', url, body);
  return data.user;
};

export { login, register };
