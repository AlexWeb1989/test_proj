import RequestAPI from './Requests';

const getBooks = async (jwt: string) => {
  const url = '/books';
  const body = undefined;
  const data = await RequestAPI('get', url, body, undefined, jwt);
  return data;
};

const getBookById = async (jwt: string, id: string) => {
  const url = `/books/${id}`;
  const body = undefined;
  const { data } = await RequestAPI('get', url, body, undefined, jwt);
  return data;
};

const searchBook = async (jwt: string, query: string) => {
  const url = '/books';
  const body = undefined;
  const { data } = await RequestAPI('get', url, body, { q: query }, jwt);
  return data;
};

export { getBooks, getBookById, searchBook };
